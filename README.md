# Callen

Project moved and renamed: https://source.puri.sm/david.hamner/ivrbbs/
This GPL3 IVR (Interactive Voice Response) is targeted at the L5 running PureOS.

# Setup:
```
sudo apt install espeak multimon-ng sox pulseaudio-utils git
git clone https://bitbucket.org/hackersgame/callen
cd callen
./callen.py
```
---
Copyright (C) 2020 David Hamner

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
